## T-encoder ##
Blacmagick SD/HD streaming encoder web interface

Requirements
------------
- Ubuntu 14.04.
- [nginx + nginx-rtmp module git version](http://nginx.org) with php5.
- twitter bootstrap + jquery.
- [libav git version](http://libav.org).
- bmdtools [lu_zero](http://github.org).
- gstreamer0.10 all plugins (not now).
- gstd git version (not now).

- Read [system] to Linux setup

kino
